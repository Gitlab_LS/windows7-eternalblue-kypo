# Windows Test KYPO

A lab that tests how to implement a Windows 7 machine (Service Pack 1) with the EternalBlue vulnerability ([CVE-2017-0144](https://cve.mitre.org/cgi-bin/cvename.cgi?name=cve-2017-0144)) to a KYPO sandbox.
